These steps can be used to build a OSGI wrapped Oracle 11g driver (see https://gist.github.com/skhatri/3016284 for Spring Roo based inspiration ).

1. Run "mvn install:install-file -Dfile=./ojdbc6.jar -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.4 -Dpackaging=jar " to install Oracle driver (e.g., ojdbc6.jar from http://www.oracle.com/technetwork/database/enterprise-edition/jdbc-112010-090769.html ) to your local maven repository, after downloading it from Oracle.

2. Update the pom file in a new osgi-ojdbc wrapper project for the version added.

3. Run "mvn clean bundle:bundle install " on the project osgi-ojdbc to create osgi jar (and install into your local maven repository).

4. Add the osgi jar to your project dependencies or install manually into the AEM web console (e.g., http://localhost:4502/system/console/bundles ).

5. Add any other OSGI dependencies for the JDBC driver.  The current Oracle 11g driver has an import reference to the org.objectweb.asm classes ( http://asm.ows2.org ). 
